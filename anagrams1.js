const button = document.getElementById("finderTool");
//Create empty results//
let results = [];



function alphabetize(a) {
  return a
    .toLowerCase()
    .split("")
    .sort()
    .join("")
    .trim();
}

function getAnagramsOf(typedText) {
  for (i = 0; i < words.length; i++) {
    if (alphabetize(typedText) === alphabetize(words[i])) {
      results.push(words[i]);
    }
  }
  return String(results).replace(/,/g, ", ");
}



button.onclick = function() {
  let typedText = document.getElementById("input").value;
  writeToPage(getAnagramsOf(typedText));
};



function writeToPage(string) {
  let span = document.createElement("span");
  let textContent = document.createTextNode(string);
  span.appendChild(textContent);
  document.body.appendChild(span);
}

